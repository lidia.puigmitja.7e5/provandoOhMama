package com.example.affirmations.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainViewModel: ViewModel() {
    // Game UI state
    private val _uiState = MutableStateFlow(Datasource().loadAffirmations())
    // Backing property to avoid state updates from other classes
    val uiState: StateFlow<List<Affirmation>> = _uiState.asStateFlow()

}