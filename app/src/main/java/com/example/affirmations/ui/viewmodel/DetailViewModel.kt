package com.example.affirmations.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class DetailViewModel: ViewModel() {

    private val _uiState = MutableStateFlow<Affirmation?>(null)
    // Backing property to avoid state updates from other classes
    val uiState: StateFlow<Affirmation?> = _uiState.asStateFlow()


    fun getAffirmation(cardID: Int) {
        // Continue picking up a new random word until you get one that hasn't been used before
        val listaAfirmations=Datasource().loadAffirmations()
        _uiState.value= listaAfirmations.firstOrNull { it.id==cardID }
    }
}