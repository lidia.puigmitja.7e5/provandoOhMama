package com.example.affirmations

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.theme.Shapes
import com.example.affirmations.ui.theme.Typography
import com.example.affirmations.ui.viewmodel.DetailViewModel

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val indice = intent.getIntExtra("indice", 0)
            DetailInformation(indice = indice)
        }
    }
}

@Composable
fun DetailInformation(indice: Int, detailViewModel: DetailViewModel = viewModel()) {
    detailViewModel.getAffirmation(indice)
    val uiState by detailViewModel.uiState.collectAsState()
    uiState?.let {
        AffirmationView(it)
    }
}


@Composable
fun AffirmationView(uiState: Affirmation) {
    AffirmationsTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxWidth(),
            color = MaterialTheme.colors.background
        ) {
            Column(
                modifier = Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

            }
            Description(affirmation = uiState)
        }
    }
}

@Composable
fun Description(affirmation: Affirmation) {

    Column(
        modifier = Modifier
            .padding(10.dp)
            .fillMaxHeight(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
    ) {
        Image(
            painter = painterResource(id = affirmation.imageResourceId),
            contentDescription = stringResource(id = affirmation.stringResourceId),
            modifier = Modifier
                .height(200.dp)
                .fillMaxHeight()
                .padding(10.dp)
                .border(1.dp, color = MaterialTheme.colors.primary, shape = Shapes.medium),
            contentScale = ContentScale.Crop

        )
        Text(
            text = "Description:",
            style = Typography.body1,
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .padding(10.dp)
        )

        Text(
            text = stringResource(id = affirmation.stringDescriptionId),
            style = Typography.body1,
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .padding(10.dp),
            overflow = TextOverflow.Ellipsis,
            maxLines = 2

        )
        Spacer(modifier = Modifier.height(10.dp))
        SocialIcons(affirmation)

    }


}

@Composable
fun SocialIcons(affirmation: Affirmation) {

    Row(modifier = Modifier
        .padding(10.dp)
        .fillMaxWidth(), horizontalArrangement = Arrangement.End, verticalAlignment = Alignment.Bottom) {
        val cntx = LocalContext.current
       // val to=stringResource(affirmation.stringTo)
       // val subject=stringResource(affirmation.stringSubject)
       // val phone=stringResource(affirmation.stringTelephone)
        IconButton(onClick = {
            cntx.sendMail(
                to=cntx.getString(affirmation.stringTo), subject = cntx.getString(affirmation.stringSubject))
        }) {
            Icon(
                imageVector = androidx.compose.material.icons.Icons.Filled.Mail,
                tint = MaterialTheme.colors.secondary,
                contentDescription = "MailIcon"
            )
        }
        Spacer(modifier = Modifier.width(10.dp))
        IconButton(onClick = {
            cntx.dial(phone = cntx.getString(affirmation.stringTelephone))
        }) {
            Icon(
                imageVector = androidx.compose.material.icons.Icons.Filled.Phone,
                tint = MaterialTheme.colors.secondary,
                contentDescription = "PhoneIcon"
            )
        }
    }
}

private fun Context.sendMail(to: String, subject: String) {
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //startActivity(intent)
        startActivity(Intent.createChooser(intent,"Choose an Email client: "))
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
    } catch (t: Throwable) {
        Toast.makeText(this, t.toString(), Toast.LENGTH_LONG).show()
    }
}

private fun Context.dial(phone: String) {
    try {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent)
    } catch (t: Throwable) {
        Toast.makeText(this, t.toString(), Toast.LENGTH_LONG).show()
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    AffirmationsTheme {
        AffirmationView(Datasource().loadAffirmations().last())
    }
}