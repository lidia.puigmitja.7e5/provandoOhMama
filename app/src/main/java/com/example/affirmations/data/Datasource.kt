/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.data
import com.example.affirmations.R
import com.example.affirmations.model.Affirmation
/**
 * [Datasource] generates a list of [Affirmation]
 */
class Datasource() {
    fun loadAffirmations(): List<Affirmation> {
        return listOf<Affirmation>(
            Affirmation(1231,R.string.affirmation1, R.drawable.image1, R.string.description1, R.string.mail1,R.string.subject1,R.string.phone1),
            Affirmation(1232,R.string.affirmation2, R.drawable.image2, R.string.description2, R.string.mail2,R.string.subject2,R.string.phone2),
            Affirmation(1233,R.string.affirmation3, R.drawable.image3, R.string.description3, R.string.mail3,R.string.subject3,R.string.phone3),
            Affirmation(1234,R.string.affirmation4, R.drawable.image4, R.string.description4, R.string.mail4,R.string.subject4,R.string.phone4),
            Affirmation(1235,R.string.affirmation5, R.drawable.image5, R.string.description5, R.string.mail5,R.string.subject5,R.string.phone5),
            Affirmation(1236,R.string.affirmation6, R.drawable.image6, R.string.description6, R.string.mail6,R.string.subject6,R.string.phone6),
            Affirmation(1237,R.string.affirmation7, R.drawable.image7, R.string.description7, R.string.mail7,R.string.subject7,R.string.phone7),
            Affirmation(1238,R.string.affirmation8, R.drawable.image8, R.string.description8, R.string.mail8,R.string.subject8,R.string.phone8),
            Affirmation(1239,R.string.affirmation9, R.drawable.image9, R.string.description9, R.string.mail9,R.string.subject9,R.string.phone9),
            Affirmation(1240,R.string.affirmation10, R.drawable.image10, R.string.description10, R.string.mail10,R.string.subject10,R.string.phone10))
    }
}
