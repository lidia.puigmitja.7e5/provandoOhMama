package com.example.affirmations.ui.tests

import com.example.affirmations.R
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.viewmodel.MainViewModel
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class MainViewModelTest {

    private val viewModel= MainViewModel()

    @Test
    fun mainViewModel_getAffirmation_ErrorFlagUnset(){

        val listaAffirmations=viewModel.uiState.value
        val correctListAffirmations=  Datasource().loadAffirmations()

        val   lista= listOf<Affirmation>(
        Affirmation(1231,R.string.affirmation1, R.drawable.image1, R.string.description1, R.string.mail1,R.string.subject1,R.string.phone1),
        Affirmation(1232,R.string.affirmation2, R.drawable.image2, R.string.description2, R.string.mail2,R.string.subject2,R.string.phone2),
        Affirmation(1233,R.string.affirmation3, R.drawable.image3, R.string.description3, R.string.mail3,R.string.subject3,R.string.phone3))


        //Assert that affirmation correct is the affirmation with this id
        println(listaAffirmations)
        println(correctListAffirmations)
        assertEquals(listaAffirmations, correctListAffirmations)
        assertNotEquals(listaAffirmations, lista)
    }
}