package com.example.affirmations.ui.tests

import com.example.affirmations.R
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.viewmodel.DetailViewModel
import junit.framework.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class DetailViewModelTest {
    private val viewModel= DetailViewModel()

    @Test
    fun detailViewModel_getAffirmation_ErrorFlagUnset(){
        viewModel.getAffirmation(1232)
        val affirmation=viewModel.uiState.value
        val correctAffirmation=  Affirmation(1232,
            R.string.affirmation2, R.drawable.image2, R.string.description2, R.string.mail2,
            R.string.subject2,
            R.string.phone2)

        val incorrectAffirmation= Affirmation(1231,R.string.affirmation1, R.drawable.image1, R.string.description1, R.string.mail1,R.string.subject1,R.string.phone1)

        //Assert that affirmation correct is the affirmation with this id
        println(correctAffirmation)
        println(affirmation)
        assertEquals(correctAffirmation, affirmation)
        assertNotEquals(incorrectAffirmation,affirmation)
    }

}